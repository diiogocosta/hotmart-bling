<?php

class Cliente 
{
 public $tipoPessoa; //String
 public $endereco; //String
 public $cpf_cnpj; //String
 public $ie_rg; //String
 public $numero; //String
 public $complemento; //String
 public $bairro; //String
 public $cep; //String
 public $cidade; //String
 public $uf; //String
 public $fone; //String
 public $email; //String
}

class DadosEtiqueta
{
 public $nome; //String
 public $endereco; //String
 public $numero; //String
 public $complemento; //String
 public $municipio; //String
 public $uf; //String
 public $cep; //String
 public $bairro; //String
}

class Volume
{
 public $servico; //String
}

class Volumes
{
 public $volume; //array(Volume)
}

class Transporte
{
 public $transportadora; //String
 public $tipo_frete; //String
 public $servico_correios; //String
 public $dados_etiqueta; //DadosEtiqueta
 public $volumes; //Volumes
}

class Item
{
 public $codigo; //String
 public $descricao; //String
 public $un; //String
 public $qtde; //String
 public $vlr_unit; //String
}

class Itens
{
 public $item; //array(Item)
}

class Parcela
{
 public $data; //String
 public $vlr; //String
 public $obs; //String
}

class Parcelas
{
 public $parcela; //array(Parcela)
}

class Pedido
{
 public $numero;
 public $numero_loja;
 public $data;
 public $cliente; //Cliente
 public $transporte; //Transporte
 public $itens; //Itens
 public $parcelas; //Parcelas
 public $vlr_frete; //String
 public $vlr_desconto; //String
 public $obs; //String
 public $obs_internas; //String
 PUBLIC $vendedor;
}

?>