<?php
require_once('classes-bling.php');
require_once('bling.php');
require_once('SimpleOrm.class.php');

class Pedidos extends SimpleOrm {};

$data = (object) $_POST;
$status = '';
$idPedido = 6;

file_put_contents('payload-hotmart.txt',json_encode($_POST));

switch ($data->status) {
    case 'started':        
        $idPedido = 21;
        break;
    case 'approved':        
        $idPedido = 6; 
        break;        
    case 'completed':   
        $idPedido = 9;                  
        break;     
    case 'billet_printed':         
        $idPedido = 6;  
        break;   
    case 'pending_analysis':   
        $idPedido = 6;    
        break;           
    case 'canceled':        
        $idPedido = 12;
        break;       
    case 'refunded':   
        $idPedido = 12;     
        break;      
    case 'dispute':      
        $idPedido = 12;      
        break;           
    case 'blocked':      
        $idPedido = 12;      
        break;                   
    case 'chargeback':      
        $idPedido = 12;      
        break;                 
    case 'delayed':      
        $idPedido = 9;     
        break;                        
    case 'expired':      
        $idPedido = 12;     
        break;                   
    default:               
        break;
}

$item = new Item();
$item->codigo = $data->prod;
$item->descricao = $data->prod_name;
$item->qtde = '1';
$item->vlr_unit = $data->full_price;

$cliente = new Cliente();
$cliente->nome = $data->name;
$cliente->cpf_cnpj = $data->doc;
$cliente->bairro = $data->address_district;
$cliente->cep = $data->address_zip_code;
$cliente->cidade = $data->address_city;
$cliente->uf = $data->address_state;
$cliente->numero = $data->address_number;
$cliente->endereco = $data->address;
$cliente->complemento = $data->address_comp;
$cliente->fone = $data->phone_local_code.$data->phone_number;
$cliente->email = $data->email;

$pedido = new Pedido();
$pedido->cliente = (array) $cliente;
$pedido->itens = array('item'=> (array)$item);
$pedido->obs = 'COMPRA EFETUADA PELO HOTMART, TRANSAÇÃO: '.$data->transaction;
$pedido->numero_loja = $data->transaction; 
$pedido->vendedor = 'HOTMART';

$xml_pedido = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><pedido></pedido>');    

array_to_xml((array)$pedido,$xml_pedido);
$xml = $xml_pedido->asXML();

$params = parse_ini_file(sprintf('%s/parameters.ini', __DIR__), true);

$conn = new mysqli($params['database']['host'], $params['database']['user'], $params['database']['password']);

if ($conn->connect_error)
die(sprintf('Unable to connect to the database. %s', $conn->connect_error));

SimpleOrm::useConnection($conn, $params['database']['name']);

$entry = Pedidos::retrieveByTransacao_hotmart($data->transaction, SimpleOrm::FETCH_ONE);

if (!$entry){
    $entry = new Pedidos;
    $retorno = json_decode(postBling($xml));                
    $entry->transacao_hotmart = $data->transaction;
    $entry->pedido_bling = $retorno->retorno->pedidos[0]->pedido->numero;    
    $entry->save();
    $xml_pedido->asXML('arquivo.xml');
} else {        
    $xml_pedido = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><pedido></pedido>');    
    array_to_xml(array('idSituacao'=>$idPedido),$xml_pedido);    
    $xml = $xml_pedido->asXML();    
    $retorno = putBling($xml,$entry->pedido_bling);    
    $xml_pedido->asXML('arquivo.xml');
}
?>