<?php    

 $apikey = "9f13658bf22029cca98a80b7e35d93f7c77bb39f5df8d8ffac13fb6ce36deb98e327dc7e";

function array_to_xml($array, &$xml_pedido) {    
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml_pedido->addChild("$key");
                array_to_xml($value, $subnode);
            }else{
                $subnode = $xml_pedido->addChild("item$key");
                array_to_xml($value, $subnode);
            }
        }else {
            $xml_pedido->addChild("$key",htmlspecialchars("$value"));
        }
    }
}

function postBling($xml){   
    global $apikey;    
    $url = 'https://bling.com.br/Api/v2/pedido/json/';
    $posts = array (
        "apikey" => $apikey,
        "xml" => rawurlencode($xml)
    );
    return executeHttp('post',$url, $posts);    
}

function putBling($xml,$nrPedido){    
    global $apikey;
    $url = 'https://bling.com.br/Api/v2/pedido/'.$nrPedido.'/json';
    $posts = array (
        "apikey" => $apikey,
        "xml" => rawurlencode($xml)
    );
    return executeHttp('put',$url, $posts);    
}

function executeHttp($method, $url, $data){
    $curl_handle = curl_init();
    curl_setopt($curl_handle, CURLOPT_URL, $url);   

    if ($method == 'post'){
        curl_setopt($curl_handle, CURLOPT_POST, count($data));
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);    
    }

    if ($method == 'put'){
        curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl_handle, CURLOPT_POST, count($data));
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
    }

    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
    $response = curl_exec($curl_handle);
    curl_close($curl_handle);
    return $response;
}

?>